package biz.stillhart.secret.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import biz.stillhart.secret.R;
import biz.stillhart.secret.models.Suggestions;

/**
 * Created by Patrick Stillhart on 22.05.2016.
 * displays a suggestions list
 */
public class SuggestionAdapter extends BaseAdapter {

    private Activity activity;
    private Suggestions suggestions;

    public SuggestionAdapter(Activity activity, Suggestions suggestions) {
        this.activity = activity;
        this.suggestions = suggestions;
    }

    @Override
    public int getCount() {
        return suggestions.getSize();
    }

    @Override
    public String getItem(int position) {
        return suggestions.getLatest().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = activity.getLayoutInflater().inflate(R.layout.item_list, parent, false);

        final String entry = getItem(position);

        final TextView letterView = (TextView) convertView.findViewById(R.id.letter);
        TextView titleView = (TextView) convertView.findViewById(R.id.title);
        RelativeLayout lockLetter = (RelativeLayout) convertView.findViewById(R.id.lockLetter);

        letterView.setText((entry.charAt(0) + "").toUpperCase());
        titleView.setText(entry);
/*
        lockLetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setTextColor(activity.getResources().getColor(R.color.colorGray));
            }
        });
*/
        return convertView;
    }
}
