package biz.stillhart.secret.utils;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * Does some password magic.
 */
public class PasswordGenerator {

    public static void main(String[] args) {

        int[] keys = new int[]{0,0, 0,0};
        String salt = "100000";
        String[] tests = new String[]{"google", "googl", "googlf", "creditsuisse", "bzz"};


        long startTime;
        String result;
        for (String test : tests) {

            startTime = System.currentTimeMillis();
            result = PasswordGenerator.generate(test, salt, keys, 10);
            System.out.println(result + " in " + (System.currentTimeMillis() - startTime) + " ms");

        }

    }

    // private final static int PW_LENGTH = 10;
    private final static int MIN_EACH_IN_PW = 2;

    private final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private final static char[] LOWER_CASE_PALLET = ALPHABET.toLowerCase().toCharArray();
    private final static char[] UPPER_CASE_PALLET = ALPHABET.toUpperCase().replace("O", "").toCharArray();

    private final static char[] NUMBER_PALLET = "123456789".toCharArray();

    private final static char[] SPECIAL_PALLET = ",;.:-_$!?=)(/&%*+".toCharArray();


    public static String generate(String raw, String salt, int[] keys, int raw_pw_length) {
        int pw_length = Math.max(2, Math.abs(raw_pw_length));
        int salty = Integer.parseInt(salt);

        for(int i = 0; i < keys.length; i++) {
            if(keys[i] == 0) keys[i] = (i + 1) * raw.length() % 10;
         }

        // make hash
        long hash = keys[0] * 11 + keys[1];
        for (int i = 0; i < raw.length(); i++) {
            hash = hash * 31 + raw.charAt(i) * keys[i % 4];
        }
        hash *= salty;

        // make hash long enough
        char[] order = new char[pw_length * 3];
        StringBuilder tmp = new StringBuilder(pw_length * 3);
        for (int offset = 0; offset < order.length; ) {
            tmp.setLength(0);
            tmp.append(Math.abs(hash * salty));
            tmp.getChars(0, Math.min(order.length - offset, tmp.length()), order, offset);
            offset += tmp.length();
        }

        // make pw out of hash
        StringBuilder sb = new StringBuilder();
        Num sumLow = Num._0(), sumUp = Num._0(), sumNum = Num._0(), sumSP = Num._0();
        for (int i = 0, next; sb.length() < pw_length; ) {
            next = Integer.parseInt("" + order[i] + order[++i] + order[++i]);

            if ((i + order[i]) % 4 == 0) sb.append(LOWER_CASE_PALLET[map(next, 0, 999, 0, LOWER_CASE_PALLET.length - 1, ++sumLow.val)]);
            else if ((i + order[i]) % 3 == 0) sb.append(UPPER_CASE_PALLET[map(next, 0, 999, 0, UPPER_CASE_PALLET.length - 1, ++sumUp.val)]);
            else if ((i + order[i]) % 2 == 0) sb.append(SPECIAL_PALLET[map(next, 0, 999, 0, SPECIAL_PALLET.length - 1, ++sumSP.val)]);
            else sb.append(NUMBER_PALLET[map(next, 0, 999, 0, NUMBER_PALLET.length - 1, ++sumNum.val)]);
        }

        // make sure MIN_EACH_IN_PW of each array is present
        if (sumLow.val < MIN_EACH_IN_PW) replacer(sb, sumLow, LOWER_CASE_PALLET, sumNum, sumUp, sumSP, NUMBER_PALLET, UPPER_CASE_PALLET, SPECIAL_PALLET);
        if (sumUp.val < MIN_EACH_IN_PW) replacer(sb, sumUp, UPPER_CASE_PALLET, sumNum, sumLow, sumSP, NUMBER_PALLET, LOWER_CASE_PALLET, SPECIAL_PALLET);
        if (sumSP.val < MIN_EACH_IN_PW) replacer(sb, sumSP, SPECIAL_PALLET, sumNum, sumLow, sumUp, NUMBER_PALLET, LOWER_CASE_PALLET, UPPER_CASE_PALLET);
        if (sumNum.val < MIN_EACH_IN_PW) replacer(sb, sumNum, NUMBER_PALLET, sumSP, sumLow, sumUp, SPECIAL_PALLET, LOWER_CASE_PALLET, UPPER_CASE_PALLET);

        return sb.toString();
    }

    private static void replacer(StringBuilder sb, Num base, char[] replacement, Num a, Num b, Num c, char[] arrA, char[] arrB, char[] arrC) {
        char[] toRemove;

        if (a.val > MIN_EACH_IN_PW) {
            toRemove = arrA;
            --a.val;
        } else if (b.val > MIN_EACH_IN_PW) {
            toRemove = arrB;
            --b.val;
        } else if (c.val > MIN_EACH_IN_PW) {
            toRemove = arrC;
            --c.val;
        } else return;

        for (int i = 0; i < sb.length(); i++)
            for (int j = 0; j < toRemove.length; j++)
                if (toRemove[j] == sb.charAt(i)) {
                    sb.setCharAt(i, replacement[map(j, 0, toRemove.length - 1, 0, replacement.length - 1, ++base.val)]);
                    if (base.val >= MIN_EACH_IN_PW) return;
                }

    }

    private static int map(float s, float a1, float a2, float b1, float b2, int dontUse) {
        return (int) (b1 + (s - a1) * (b2 - b1) / (a2 - a1));
    }

    private static class Num {
        static Num _0() {
            return new Num();
        }
        int val = 0;
    }

}
