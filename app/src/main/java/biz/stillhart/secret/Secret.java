package biz.stillhart.secret;

import android.app.Application;
import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

/**
 * Created by Patrick Stillhart on 22.05.2016.
 * Global stuff
 */
@ReportsCrashes(
        formUri = "https://arc.cloudant.com/acra-secret/_design/acra-storage/_update/report",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.PUT,
        formUriBasicAuthLogin = "yestrandingtheryoustayet",
        formUriBasicAuthPassword = "26c434325392dea68b21ff83117994f9b08f6f08",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE
        }
)
public class Secret extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) { // remember internet-permission
            ACRA.init(this);
        }

    }


}
