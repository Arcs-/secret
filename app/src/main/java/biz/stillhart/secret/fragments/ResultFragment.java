package biz.stillhart.secret.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import biz.stillhart.secret.R;

import java.util.Random;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * Fragment showing the magic number and the restore code
 */
public class ResultFragment extends Fragment {

    public static Fragment newInstance(int[] keys) {
        ResultFragment resultFragment = new ResultFragment();

        Bundle args = new Bundle();
        args.putIntArray("keys", keys);
        resultFragment.setArguments(args);

        return resultFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_result, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView magicNumber = (TextView) view.findViewById(R.id.magicNumber),
                secret_code = (TextView) view.findViewById(R.id.secret_code),
                magicTip = (TextView) view.findViewById(R.id.magicTip);

        int[] keys = getArguments().getIntArray("keys");

        if (keys == null || keys.length != 3) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_frame, FirstFragment.newInstance())
                    .commit();
            return;
        }

        final int check = (keys[0] + keys[1] + keys[2]) % 10;
        final String salt = new Random().nextInt(99_999) + 10_000 + "";

        String checked = check + "";

        magicNumber.setText(checked);
        magicTip.setText(getString(R.string.intro_result_login, keys[0], keys[1], keys[2], check));
        secret_code.setText(salt);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:

                                SharedPreferences.Editor editor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
                                editor.putString(getString(R.string.pref_key), salt);
                                editor.putBoolean(getString(R.string.pref_secondLaunch), true);
                                editor.apply();

                                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                                if (actionBar != null) actionBar.show();

                                getFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                                        .replace(R.id.fragment_frame, LoginFragment.newInstance())
                                        .commit();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.intro_result_check))
                        .setPositiveButton(getString(R.string.intro_result_check_positive), dialogClickListener)
                        .setNegativeButton(getString(R.string.intro_result_check_negative), dialogClickListener).show();

            }
        });


    }

}
