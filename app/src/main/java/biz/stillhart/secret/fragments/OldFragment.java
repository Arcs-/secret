package biz.stillhart.secret.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import biz.stillhart.secret.R;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * Called when someone already possess a code
 */
public class OldFragment extends Fragment {

    public static Fragment newInstance() {
        return new OldFragment();
    }

    private EditText secret_code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_old, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        secret_code = (EditText) view.findViewById(R.id.secret_code);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString(getString(R.string.pref_key), secret_code.getText().toString());
                editor.putBoolean(getString(R.string.pref_secondLaunch), true);
                editor.apply();

                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                if (actionBar != null) actionBar.show();

                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.fragment_frame, LoginFragment.newInstance())
                        .commit();
            }
        });


    }

}
