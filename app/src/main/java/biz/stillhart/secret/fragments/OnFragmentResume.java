package biz.stillhart.secret.fragments;

/**
 * Created by Patrick Stillhart on 25.05.2016.
 * Implement go get called when the fragment pops back up
 */
public interface OnFragmentResume {

    void onFragmentResume();

}
