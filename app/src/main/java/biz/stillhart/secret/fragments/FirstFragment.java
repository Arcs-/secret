package biz.stillhart.secret.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import biz.stillhart.secret.R;
import biz.stillhart.secret.adapter.TextWatcherAdapter;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * Fragment displaying the very first view to the user
 */
public class FirstFragment extends Fragment {

    public static Fragment newInstance() {
        return new FirstFragment();
    }

    private EditText[] nums = new EditText[3];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.hide();

        return inflater.inflate(R.layout.fragment_first_in, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        view.findViewById(R.id.notNew).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity().getCurrentFocus() != null) imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .add(R.id.fragment_frame, OldFragment.newInstance())
                        .addToBackStack("notNew ")
                        .commit();
            }
        });

        nums[0] = (EditText) view.findViewById(R.id.inputNum1);
        nums[1] = (EditText) view.findViewById(R.id.inputNum2);
        nums[2] = (EditText) view.findViewById(R.id.inputNum3);


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (EditText num : nums) {
                    if (num.length() != 1) {
                        num.requestFocus();
                        return;
                    }
                }

                int[] keys = new int[nums.length];
                for (int i = 0; i < nums.length; i++) {
                    keys[i] = Integer.parseInt(nums[i].getText().toString());
                }

                if (getActivity().getCurrentFocus() != null) imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .add(R.id.fragment_frame, ResultFragment.newInstance(keys))
                        .addToBackStack("result")
                        .commit();


            }
        });
        TextWatcher textWatcher = new TextWatcherAdapter() {

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    for (EditText num : nums) {
                        if (num.length() != 1) {
                            num.requestFocus();
                            return;
                        }
                    }
                }
            }

        };

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.requestFocus();
                ((EditText) v).selectAll();
            }
        };

        for (EditText num : nums) {
            num.addTextChangedListener(textWatcher);
            num.setOnClickListener(onClickListener);
        }

        nums[0].requestFocus();
        imm.showSoftInput(nums[0], 0);

    }

}
