package biz.stillhart.secret.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import biz.stillhart.secret.R;
import com.github.machinarius.preferencefragment.PreferenceFragment;

/**
 * Created by Patrick Stillhart on 22.05.2016.
 * Fragment for showing Preferences
 */
public class SettingsFragment extends PreferenceFragment {

    private SharedPreferences sharedPref;
    private SharedPreferences defaultPref;

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        defaultPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        View view = super.onCreateView(inflater, container, savedInstanceState);

        TypedArray androidAttr = getActivity().getTheme().obtainStyledAttributes(new int[]{
                android.R.attr.colorBackground,
        });
        if (view != null) view.setBackgroundColor(androidAttr.getColor(0, 0xFF00FF));

        final Preference pref = getPreferenceManager().findPreference("recoverySet");
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:

                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString(getString(R.string.pref_key), defaultPref.getString("recoverySet", ""));
                                editor.apply();

                                editor = defaultPref.edit();
                                editor.putString("recoverySet", "");
                                editor.apply();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                editor = defaultPref.edit();
                                editor.putString("recoverySet", "");
                                editor.apply();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.settings_override))
                        .setPositiveButton(getString(R.string.settings_override_positive), dialogClickListener)
                        .setNegativeButton(getString(R.string.settings_override_negative), dialogClickListener).show();

                return true;
            }
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragment_settings);
        setHasOptionsMenu(true);
    }

}
