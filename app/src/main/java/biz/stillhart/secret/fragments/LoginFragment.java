package biz.stillhart.secret.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import biz.stillhart.secret.R;
import biz.stillhart.secret.adapter.TextWatcherAdapter;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * Login Fragment, where you'd enter the 3 numbers and the 1 magic number
 */
public class LoginFragment extends Fragment {

    public static Fragment newInstance() {
        return new LoginFragment();
    }

    private EditText[] nums = new EditText[4];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        nums[0] = (EditText) view.findViewById(R.id.inputNum1);
        nums[1] = (EditText) view.findViewById(R.id.inputNum2);
        nums[2] = (EditText) view.findViewById(R.id.inputNum3);
        nums[3] = (EditText) view.findViewById(R.id.inputNum4);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        TextWatcher textWatcher = new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {

                    for (EditText num : nums) {
                        if (num.length() != 1) {
                            num.requestFocus();
                            return;
                        }
                    }

                    int[] keys = new int[nums.length];
                    for (int i = 0; i < nums.length; i++) {
                        keys[i] = Integer.parseInt(nums[i].getText().toString());
                    }

                    int check = (keys[0] + keys[1] + keys[2]) % 10;
                    if (check != keys[3]) {
                        for (EditText num : nums) {
                            num.getBackground().setColorFilter(getResources().getColor(R.color.colorError), PorterDuff.Mode.SRC_IN);
                        }

                    } else {

                        for (EditText num : nums) num.getBackground().clearColorFilter();

                        if (getActivity().getCurrentFocus() != null) imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                        getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                                .replace(R.id.fragment_frame, GeneratorFragment.newInstance(keys))
                                .commit();
                    }


                }
            }

        };

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.requestFocus();
                ((EditText) v).selectAll();
            }
        };

        for (EditText num : nums) {
            num.addTextChangedListener(textWatcher);
            num.setOnClickListener(onClickListener);
        }

        nums[0].requestFocus();
        imm.showSoftInput(nums[0], 0);

    }

}
