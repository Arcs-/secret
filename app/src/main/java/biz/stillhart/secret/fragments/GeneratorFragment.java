package biz.stillhart.secret.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import biz.stillhart.secret.R;
import biz.stillhart.secret.adapter.SuggestionAdapter;
import biz.stillhart.secret.adapter.TextWatcherAdapter;
import biz.stillhart.secret.models.Suggestions;
import biz.stillhart.secret.utils.ClearEditText;
import biz.stillhart.secret.utils.PasswordGenerator;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by Patrick Stillhart on 20.05.2016.
 * The fragment for displaying passwords
 */
public class GeneratorFragment extends Fragment implements OnFragmentResume {

    public static Fragment newInstance(int[] keys) {
        GeneratorFragment generatorFragment = new GeneratorFragment();

        Bundle args = new Bundle();
        args.putIntArray("keys", keys);
        generatorFragment.setArguments(args);

        return generatorFragment;
    }

    private Suggestions suggestions;
    private SuggestionAdapter suggestionAdapter;

    private ClearEditText uri;
    private ListView suggestionView;
    private View copyBlock;
    private TextView password;

    private String salt;
    private int password_length;

    private SharedPreferences sharedPref;
    private SharedPreferences defaultPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        defaultPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return inflater.inflate(R.layout.fragment_generator, container, false);
    }

    @Override
    public void onViewCreated(View view, final Bundle savedInstanceState) {

        suggestions = Suggestions.restore("lastLookups", getActivity(), defaultPref);
        suggestionAdapter = new SuggestionAdapter(getActivity(), suggestions);

        salt = sharedPref.getString(getString(R.string.pref_key), "1");
        password_length = Integer.parseInt(defaultPref.getString("pw_length", "10"));

        uri = (ClearEditText) view.findViewById(R.id.inputURI);

        suggestionView = (ListView) view.findViewById(R.id.suggestions);
        password = (TextView) view.findViewById(R.id.passwordOut);
        copyBlock = view.findViewById(R.id.copyBlock);

        suggestionView.setAdapter(suggestionAdapter);
        suggestionView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = ((TextView) view.findViewById(R.id.title)).getText().toString();
                uri.setText(text);
            }
        });
        suggestionView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String text = ((TextView) view.findViewById(R.id.title)).getText().toString();
                suggestions.remove(text);
                suggestionAdapter.notifyDataSetChanged();
                return true;
            }
        });


        final Bundle bundle = getArguments();
        uri.addTextChangedListener(new TextWatcherAdapter() {

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {

                    showPassword(true);
                    String generatedPassword = PasswordGenerator.generate(uri.getText().toString(), salt, bundle.getIntArray("keys"), password_length);
                    password.setText(generatedPassword);

                } else {
                    showPassword(false);
                }
            }

        });

        copyBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(password.getText(), password.getText());
                clipboard.setPrimaryClip(clip);

                suggestions.add(uri.getText().toString());
                suggestionAdapter.notifyDataSetChanged();
                Suggestions.save("lastLookups", getActivity(), suggestions);

                Toast.makeText(getContext(), getString(R.string.generator_copied), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onFragmentResume() {
        salt = sharedPref.getString(getString(R.string.pref_key), "1");
        password_length = Integer.parseInt(defaultPref.getString("pw_length", "10"));

        suggestions.update();
        suggestionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();

        Suggestions.save("lastLookups", getActivity(), suggestions);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_frame, LoginFragment.newInstance())
                .commit();
    }

    private void showPassword(boolean show) {
        if (show) {
            suggestionView.setVisibility(View.GONE);
            copyBlock.setVisibility(View.VISIBLE);
        } else {
            copyBlock.setVisibility(View.GONE);
            suggestionView.setVisibility(View.VISIBLE);

        }
    }

}
