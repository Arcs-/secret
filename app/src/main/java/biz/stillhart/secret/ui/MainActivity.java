package biz.stillhart.secret.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import biz.stillhart.secret.R;
import biz.stillhart.secret.fragments.FirstFragment;
import biz.stillhart.secret.fragments.LoginFragment;
import biz.stillhart.secret.fragments.OnFragmentResume;
import biz.stillhart.secret.fragments.SettingsFragment;

/**
 * Created by Patrick Stillhart on 22.05.2016.
 * Well, it's the main activity
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        boolean secondLaunch = sharedPref.getBoolean(getString(R.string.pref_secondLaunch), false);

        getSupportFragmentManager().addOnBackStackChangedListener(getListener());

        Fragment next;
        if (secondLaunch) next = LoginFragment.newInstance();
        else next = FirstFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_frame, next)
                .commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (getCurrentFocus() != null) imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("settings")
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .add(R.id.fragment_frame, new SettingsFragment())
                    .commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    Kind of buggy... value returned by `manager.getBackStackEntryCount()? doesn't match `getFragments()`
     */
    private FragmentManager.OnBackStackChangedListener getListener() {
        return new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {

                FragmentManager manager = getSupportFragmentManager();
                if (manager != null) {

                    Fragment currentFragment = manager.getFragments().get(manager.getFragments().size() - 1);
                    if (currentFragment instanceof OnFragmentResume) ((OnFragmentResume) currentFragment).onFragmentResume();

                }

            }
        };
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) getFragmentManager().popBackStack();
        else super.onBackPressed();
    }


}
