package biz.stillhart.secret.models;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Patrick Stillhart on 22.05.2016.
 * The suggestion list
 */
public class Suggestions implements Serializable {

    public static void save(String key, Activity activity, Suggestions suggestions) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(suggestions);
            so.flush();

            SharedPreferences.Editor editor = activity.getPreferences(Context.MODE_PRIVATE).edit();
            editor.putString(key, Base64.encodeToString(bo.toByteArray(), Base64.DEFAULT));
            editor.apply();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Suggestions restore(String key, Activity activity, SharedPreferences defaultPref) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        String serializedSuggestions = sharedPref.getString(key, null);
        if (serializedSuggestions == null) return new Suggestions(defaultPref);

        try {

            ByteArrayInputStream bi = new ByteArrayInputStream(Base64.decode(serializedSuggestions, Base64.DEFAULT));
            ObjectInputStream si = new ObjectInputStream(bi);

            Suggestions suggestions = (Suggestions) si.readObject();
            suggestions.updatePref(defaultPref);

            return suggestions;

        } catch (IOException | ClassNotFoundException e) {
            //ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
            return new Suggestions(defaultPref);
        }

    }

    private transient SharedPreferences defaultPref;
    private boolean hiddenMode;
    private int maxLength;
    private ArrayList<String> latest;

    public Suggestions(SharedPreferences defaultPref) {
        this.defaultPref = defaultPref;
        this.latest = new ArrayList<>();
        update();
    }

    public void add(String service) {
        if (service.length() == 0 || hiddenMode) return;

        if (!latest.remove(service)) if (latest.size() >= maxLength) latest.remove(latest.size() - 1);
        latest.add(0, service);
    }

    public void remove(String service) {
        latest.remove(service);
    }

    public int getSize() {
        return latest.size();
    }

    public ArrayList<String> getLatest() {
        return latest;
    }

    private void updatePref(SharedPreferences preferences) {
        defaultPref = preferences;
    }

    public void update() {
        this.hiddenMode = !defaultPref.getBoolean("showSuggestions", true);
        if (this.hiddenMode) this.latest.clear();

        this.maxLength = Integer.parseInt(defaultPref.getString("suggestionAmount", "6"));
        if (latest.size() >= this.maxLength) latest.subList(this.maxLength, latest.size()).clear();
        this.latest.ensureCapacity((this.hiddenMode) ? 0 : maxLength + 1);


    }
}
